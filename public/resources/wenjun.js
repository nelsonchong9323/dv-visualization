$(document).ready(function () {
    var radioValue = $("input[name='year']:checked").val();
    
    drawincome(radioValue);
    drawregion(radioValue);

    $('.radio-custom input').on('change', function () {
        radioValue = $("input[name='year']:checked").val();
        
        console.log(radioValue);
        $('#graph1').empty();
        drawincome(radioValue);
        $('#graph2').empty();
        drawregion(radioValue);
    })
})



function drawincome(year) {
    var margin = {
            top: 60,
            right: 100,
            bottom: 250,
            left: 80
        },
        width = 900 - margin.left - margin.right,
        height = 700 - margin.top - margin.bottom;

    var x = d3.scaleBand().rangeRound([0, width]).padding(0.1)
    var y = d3.scaleLinear().range([height, 0]);
    var colorscale = d3.scaleLinear()
        .domain([80, 0])
        .range(['#800000', '#FFA07A']);


    var svg = d3.select("#graph1")
        .append("svg")
        .style("width", width + margin.left + margin.right + "px")
        .style("height", height + margin.top + margin.bottom + "px")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
        .attr("class", "svg");


    d3.csv("data/data.csv", function (error, data) {
        if (error) throw error;


        data.forEach(function (d) {
            d[year] = +d[year];
            d.IncomeGroup = d.IncomeGroup;
        });

        var nest = d3.nest()
            .key(function (d) {
                return d.IncomeGroup;
            })
            .sortKeys(d3.ascending)
            .rollup(function (leaves) {
                return {
                    "count": leaves.length,
                    "total": d3.sum(leaves, function (d) {
                        return (d[year])
                    })
                };
            })
            .entries(data)

        console.log(nest.map(function (d) {
            return d.value.total;

        }))

        x.domain(nest.map(function (d) {
            return d.key;
        }));
        y.domain([0, d3.max(nest, function (d) {
            return d.value.total / d.value.count;
        }) + 5]);


        var xaxis = svg.append("g")
            .attr("transform", "translate(0," + height + ")")
            .attr("class", "x axis")
            .call(d3.axisBottom(x)

                .tickSize(0, 0)

                .tickSizeInner(0)
                .tickPadding(10))
            .selectAll("text")
            .attr("dx", "-5.8em")
            .attr("dy", ".15em")
            .attr("transform", "rotate(-65)");


        var yaxis = svg.append("g")
            .attr("class", "y axis")
            .call(d3.axisLeft(y)
                .ticks(5)
                .tickSizeInner(0)
                .tickPadding(6)
                .tickSize(0, 0));

        svg.append("text")
            .attr("transform", "rotate(-90)")
            .attr("y", 0 - 60)
            .attr("x", 0 - (height / 2))
            .attr("dy", "1em")
            .style("text-anchor", "middle")
            .text(" Emmision metrics ton per capita")
            .attr("class", "y axis label");

        // Draw the bars
        svg.selectAll(".rect")
            .data(nest)
            .enter()
            .append("rect")
            .attr("class", "bar")
            .attr("x", function (d) {
                return x(d.key);
            })
            .attr("y", function (d) {
                return y(d.value.total / d.value.count);
            })
            .attr("fill", function (d) {
                return colorscale(d.value.total / d.value.count);
            })
            .attr("width", x.bandwidth())
            .attr("height", function (d) {
                return height - y(d.value.total / d.value.count);
            });

        svg.append("text")
            .attr("x", (width / 2))
            .attr("y", 0 - (margin.top / 2))
            .attr("text-anchor", "middle")
            .style("font-size", "16px")
            .style("text-decoration", "Bold")
            .text("Average CO2 emission based on Income on " + year);
    })

}

function drawregion(year) {
    var margin = {
            top: 60,
            right: 100,
            bottom: 250,
            left: 80
        },
        width = 900 - margin.left - margin.right,
        height = 700 - margin.top - margin.bottom;

    var x = d3.scaleBand().rangeRound([0, width]).padding(0.1)
    var y = d3.scaleLinear().range([height, 0]);
    var colorscale = d3.scaleLinear()
        .domain([80, 0])
        .range(['#800000', '#FFA07A']);


    var svg = d3.select("#graph2")
        .append("svg")
        .style("width", width + margin.left + margin.right + "px")
        .style("height", height + margin.top + margin.bottom + "px")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
        .attr("class", "svg");


    d3.csv("data/data.csv", function (error, data) {
        if (error) throw error;

        data.forEach(function (d) {

            d[year] = +d[year];
            d.Region = d.Region;
        });

        var nest = d3.nest()
            .key(function (d) {
                return d.Region;
            })
            .sortKeys(d3.ascending)
            .rollup(function (leaves) {
                return {
                    "count": leaves.length,
                    "total": d3.sum(leaves, function (d) {
                        return (d[year])
                    })
                };
            })
            .entries(data)

        console.log(nest.map(function (d) {
            return d.value.total;

        }))

        x.domain(nest.map(function (d) {
            return d.key;
        }));
        y.domain([0, d3.max(nest, function (d) {
            return d.value.total / d.value.count;
        })]);


        var xaxis = svg.append("g")
            .attr("transform", "translate(0," + height + ")")
            .attr("class", "x axis")
            .call(d3.axisBottom(x)

                .tickSize(0, 0)

                .tickSizeInner(0)
                .tickPadding(10))
            .selectAll("text")
            .attr("dx", "-6.8em")
            .attr("dy", ".15em")
            .attr("transform", "rotate(-65)");


        var yaxis = svg.append("g")
            .attr("class", "y axis")
            .call(d3.axisLeft(y)
                .ticks(5)
                .tickSizeInner(0)
                .tickPadding(6)
                .tickSize(0, 0));




        svg.append("text")
            .attr("transform", "rotate(-90)")
            .attr("y", 0 - 60)
            .attr("x", 0 - (height / 2))
            .attr("dy", "1em")
            .style("text-anchor", "middle")
            .text(" Emmision metrics ton per capita")
            .attr("class", "y axis label");


        svg.selectAll(".rect")
            .data(nest)
            .enter()
            .append("rect")
            .attr("class", "bar")
            .attr("x", function (d) {
                return x(d.key);
            })
            .attr("y", function (d) {
                return y(d.value.total / d.value.count);
            })
            .attr("fill", function (d) {
                return colorscale(d.value.total / d.value.count);
            })
            .attr("width", x.bandwidth())
            .attr("height", function (d) {
                return height - y(d.value.total / d.value.count);
            });

        svg.append("text")
            .attr("x", (width / 2))
            .attr("y", 0 - (margin.top / 2))
            .attr("text-anchor", "middle")
            .style("font-size", "16px")
            .style("text-decoration", "Bold")
            .text("Average CO2 emission each region on " + year);


    })

}
